import React from "react";

import "./styles/style.css";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Container from "./components/Container/Container";

function App() {

  return (
    <div>
        <Header />
        <Container />
        <Footer />
    </div>
  );
}

export default App;

//y -  inputs to create our notes
//y -  note component
//y -  store our notes in an array and manage them with state
//y -  be able to delete notes
//y -  pick a color for our notes
//y -  edit our notes
// pinn main notes
// change the order of the notes
